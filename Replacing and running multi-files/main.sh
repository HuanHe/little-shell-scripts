#! /bin/bash 
# Batch processing  in  different  folders  and  running  multi-files
cd /home/hh/ovitoscript/final1

# Initialization  all files  to  a type of  "XXX/1"
a=900/10
b=900/1
sed -i "s:$a:$b:g"  `grep $a -rl /home/hh/ovitoscript/final1`  

for i in {1..9}  
do  
  for file  in  $(ls *)                    # All files in  one  folder
    do  
    ovitos  ./$file              # ovito is just a code to running my files
    done
  j=$[${i}+1]
  a=900/${i}
  b=900/${j}
 sed -i "s:$a:$b:g"  `grep $a -rl /home/hh/ovitoscript/final1`  
done   